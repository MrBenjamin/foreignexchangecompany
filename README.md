# ForeignExchangeCompany

This project leverages EC2 to make API calls to get foreign exchange rates and stores the results in an S3 bucket 

## Problem Statement 
I am starting an Exchange Company, and I want to get the rate at which exchange rate fluctuates from USD to (insert your currency of choice) 
I want to get and export of the exchange rate as a JSON output every hour into an S3 bucket. 
I also want to be notified once this is completed. 


- EC2: Responsible for running a python script that will generate exchange rates and write the output into a JSON file and send that JSON file into s3(this must be done hourly)

- S3, JSON files of the exchange rate will be stored into an s3 bucket.
IAM, EC2 will need permission to write into the bucket where exchange rate JSON files will be stored.

- SNS, we will be sending a job failure or job success notification to the exchange team each time I successfully write into the bucket or each time the jobs fails.

- JOB failure will mean error encountered in the process any point in time.

