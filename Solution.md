# Problem Statement
I am starting an Exchange Company, and I want to get the rate at which exchange rate fluctuates from USD to (insert your currency of choice) 
I want to get and export of the exchange rate as a JSON output every hour into an S3 bucket. 
I also want to be notified once this is completed. 

- EC2: Responsible for running a python script that will generate exchange rates and write the output into a JSON file and send that JSON file into s3(this must be done hourly)

- S3, JSON files of the exchange rate will be stored into an s3 bucket.

- IAM, EC2 will need permission to write into the bucket where exchange rate JSON files will be stored.

- SNS, we will be sending a job failure or job success notification to the exchange team each time I successfully write into the bucket or each time the jobs fails.

- JOB failure will mean error encountered in the process any point in time.

# Architecture
![Architecture Diagram](https://gitlab.com/MrBenjamin/foreignexchangecompany/-/raw/main/Project%20Solution/Untitled_Diagram-2.jpg?inline=false)

# Solution
In order to complete this project please ensure you have access to an [Amazon Web Service](https://aws.amazon.com) account

## Step 1
- log into your [Amazon Web Service](https://aws.amazon.com) account

## Step 2 - Creating your EC2 Instance
- In your [Amazon Web Service](https://aws.amazon.com) account search for EC2 
- In the EC2 dashboard select launch instance 
- Configure your EC2 with the required configurations
    -   `Select the AMI (Amazon Machine Image` - For this project I used Amazon Linux
    -   `Select the instance type` - For this project I used t2 micro
    -   `Add Storgae`
    -   `Configure your security groups` 
    -   `Review and launch` 
- Assign an IAM role that would let your EC2 Instance write files to S3
    -   `Search IAM in the search bar for services`
    -   `In the IAM dahboard create new role`
    -   `Select EC2 Service`
    -   `Attatch the policy for this role which is to allow write access for S3` 
    -   `Review and create` 
    -   `Go back to your instance in EC2`
    -   `Select your instance and click on the Actions drop down meny`
    -   `Select security and then choose modify IAM role. Choose the role you just created`   

## Step Set Up Your S3 Bucket 
-   Search for S3 in the services search bar 
-   In S3, create a new bucket (project2Exchangerate) to store your .json files

## Set Up You Instance 
-   SSH into your EC2 instance 
-   Once in your EC2 instance create two folders (One for your scripts, another one for the json files)
-   In your Script folder, create you python script to make API calls to get exchange rate. See Python code below:

```
import forex_python
from forex_python.converter import CurrencyRates
c = CurrencyRates()
print(c.get_rates('USD'))
```
-   Run your python script. If you get an error message, check to see if your instance has the forex-python module installed.
-   Once you confirm your output, write the output to file in the folder created for json files.
-   In order to make each of these files unique to avoid overighting each other, create a shell scrip. See script below:
```
#! /bin/bash 
today=$(date +"%Y_%m_%d_%I_%M_%p")
#Pass the today value to the file name
file_name="/home/ec2-user/Project2/Data/Output-${today}.json"
#Output the result of your python script to the file name
python3 /home/ec2-user/Project2/Script/exchangerate.py >> $file_name

#copy files to S3
aws s3 cp $file_name s3://project2exchangerate

echo "I have copied the files to Project2exchagerate"
```
-   Confirm the shell script runs succesfully. 
-   Create a cron job to automate the shell script every hour. See Crontab below:
```
0 * * * * sh /home/ec2-user/Project2/Script/run.sh >> /home/ec2-user/Project2/Script/cronlog.txt
```

## Create SNS Topic
-   In the services search bar, search for sns 
-   At the SNS dashboard, create a new topic
-   Grant the SNS topic the right permissions to permit S3 send out notifcation alerts for new files writen to your S3 bucket. See Topic policy
```
{
 "Version": "2008-10-17",
 "Id": "example-ID",
 "Statement": [
  {
   "Sid": "s3-event-notifier",
   "Effect": "Allow",
   "Principal": {
     "Service": "s3.amazonaws.com"
   },
   "Action": [
    "SNS:Publish"
   ],
   "Resource": "arn:aws:sns:us-east-1:987243600493:Project2ExchangeRate",
   "Condition": {
      "ArnLike": {          
      "aws:SourceArn": "arn:aws:s3:::project2exchangerate"
    }
   }
  }
 ]
}
```
- Subscribe to this topic with your email and confirm subscription from the email sent 

## Create S3 Event Notification
-   Return back to S3
-   Click on the bucket created `project2exchangerate`
-   Click on properties and scroll down to Event Notifications and click edit 
-   Name this event notification and select Event Types (For this project we used Object Creation > Put)
-   Select destination as SNS topic
-   Select the SNS topic you created or enter sns arn
